.data
i_message: .asciiz "input a number: "
o_message: .asciiz "N! of the input number is: "
e_message: .asciiz "negative input, please try again.\n"

.text
.globl main
main:
li $v0,4  
la $a0,i_message
syscall
li $v0,5
syscall
add $t0,$v0,$0
bgez $t0,next
li $v0,4
la $a0, e_message
syscall
j main

next:
addi $sp,$sp,-4
sw $ra,4($sp)
add $a0,$t0,$0
jal Factoral
add $t0, $v0,$0
li $v0,4
la $a0,o_message
syscall
li $v0,1
add $a0,$t0,$0
syscall
lw $ra,4($sp)
addi $sp, $sp,4
jr $ra

Factoral:
addi $sp,$sp,-4
sw $ra, 4($sp)
beqz $a0,end
addi $sp,$sp,-4
sw $a0,4($sp)
add $a0,$a0,-1
jal Factoral
lw $t0,4($sp)
mul $v0,$v0,$t0
lw $ra,8($sp)
addi $sp,$sp,8
jr $ra

end:
li $v0,1
lw $ra, 4($sp)
addi $sp,$sp,4
jr $ra