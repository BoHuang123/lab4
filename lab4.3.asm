.data
message: .asciiz "the largest number is: "
.text
.globl main
main:
li $v0,5
syscall
move $t0,$v0
li $v0,5
syscall
add $t1,$v0,$0
addi $sp, $sp, -4
sw $ra, 4($sp)
move $a0,$t0
move $a1, $t1
jal largest
lw $ra, 4($sp)
addi $sp, $sp, 4
jr $ra

largest:
move $t0,$a0
li $v0,4
la $a0, message
syscall
add $a0,$t0,$0
slt $t0,$a0,$a1
blez $t0,output
move $a0,$a1

output:
li $v0,1
syscall
jr $ra