.data
message: .asciiz "the largest number is: "
.text
.globl main
main:
li $v0,5
syscall
add $t0,$v0,$0
li $v0,5
syscall
add $t1,$v0,$0

addi $sp, $sp, -4
sw $ra, 4($sp)

addi $sp,$sp,-8
sw $t0,4($sp)
sw $t1,8($sp)
jal largest
addi $sp, $sp,8
lw $ra,4($sp)
addi $sp,$sp,4
jr $ra

largest:

li $v0,4
la $a0, message
syscall
lw $a0,4($sp)
lw $a1,8($sp)
slt $t0,$a0,$a1
blez $t0,output
add $a0,$a1,$0

output:
li $v0,1
syscall
jr $ra