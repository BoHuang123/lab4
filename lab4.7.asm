.data
i_txt: .asciiz "input 2 non-gegative integers (input the 1st and then press enter and then input the 2nd:"
o_txt: .asciiz "the answer is:"
e_txt: .asciiz "negative numbers as input, try again.\n"

.text
.globl main
main:
	li $t6,0    #t6 is used to count how many times ack is called
	li $v0,4
	la $a0,i_txt
	syscall
	li $v0,5
	syscall
	add $t0,$v0,$0
	li $v0,5
	syscall
	add $t1, $v0, $0
	bltz $t0,wronginput
	bltz $t1,wronginput
	j OK2GO
wronginput:
	li $v0,4
	la $a0,e_txt
	syscall
	j main
OK2GO:
	li $v0,4
	la $a0,o_txt
	syscall
	
	addi $sp,$sp,-4
	sw $ra,4($sp)
	add $a0,$t0,$0
	add $a1,$t1,$0
	jal ack
	add $a0,$v0,$0
	li $v0,1
	syscall
	lw $ra,4($sp)
	addi $sp,$sp,4
	jr $ra
ack:
	addi $t6,$t6,1    #t6 is used to count how many times ack is called
	beq $a0,$0,xis0
	addi $sp,$sp,-4   #make space to store ra
	sw $ra,4($sp)     #store ra
	beq $a1,$0,yis0
	addi $t0,$a0,-1
	addi $sp,$sp,-4
	sw $t0,4($sp)    #store x-1 to the stack
	addi $a1,$a1,-1
	jal ack          #call A(x,y-1)
	lw $a0,4($sp)
	addi $sp,$sp,4
	add $a1,$v0,$0
	jal ack          #call A(x-1,A(x,y-1))
	j end
	
xis0:
	addi $v0,$a1,1
	jr $ra

yis0:
	addi $a0,$a0,-1
	li $a1,1
	jal ack     #call A(x-1,1)
	
end:
	lw $ra,4($sp)
	addi $sp,$sp,4
	jr $ra
	
	